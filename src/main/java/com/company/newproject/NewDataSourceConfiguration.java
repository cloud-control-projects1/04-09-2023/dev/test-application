package com.company.newproject;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Objects;

@EnableJpaRepositories(entityManagerFactoryRef = "newDataSourceEntityManagerFactory", transactionManagerRef = "newDataSourceTransactionManager")
@Configuration
public class NewDataSourceConfiguration {

    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties("new.datasource")
    public DataSource newDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean newDataSourceEntityManagerFactory(
            @Qualifier("newDataSource") DataSource dataSource,
            EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(dataSource)
                .packages("com.company.newproject")
                .persistenceUnit("Default")
                .build();
    }

    @Bean
    public PlatformTransactionManager newDataSourceTransactionManager(
            @Qualifier("newDataSourceEntityManagerFactory") LocalContainerEntityManagerFactoryBean newDataSourceEntityManagerFactory) {
        return new JpaTransactionManager(Objects.requireNonNull(newDataSourceEntityManagerFactory.getObject()));
    }
}