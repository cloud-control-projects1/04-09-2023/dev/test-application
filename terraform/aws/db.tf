module "postgres_db" {
  source = "./db"

  name                                  = var.postgres_db_name
  engine                                = var.postgres_db_engine
  engine_version                        = var.postgres_db_engine_version
  instance_class                        = var.postgres_db_instance_class
  storage                               = var.postgres_db_storage
  user                                  = var.postgres_db_user
  password                              = var.postgres_db_password
  random_password                       = var.postgres_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.postgres_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
